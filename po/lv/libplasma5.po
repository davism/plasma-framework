# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-framework package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-framework\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-06 00:44+0000\n"
"PO-Revision-Date: 2019-12-08 12:11+0200\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 19.08.3\n"

#: declarativeimports/calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Brīvdienas"

#: declarativeimports/calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Notikumi"

#: declarativeimports/calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Jāizdara"

#: declarativeimports/calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Citi"

#: declarativeimports/calendar/qml/MonthView.qml:244
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr ""

#: declarativeimports/calendar/qml/MonthView.qml:255
#, kde-format
msgid "Previous Month"
msgstr "Iepriekšējais mēnesis"

#: declarativeimports/calendar/qml/MonthView.qml:257
#, kde-format
msgid "Previous Year"
msgstr "Iepriekšējais gads"

#: declarativeimports/calendar/qml/MonthView.qml:259
#, kde-format
msgid "Previous Decade"
msgstr "Iepriekšējā dekāde"

#: declarativeimports/calendar/qml/MonthView.qml:272
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Šodiena"

#: declarativeimports/calendar/qml/MonthView.qml:273
#, kde-format
msgid "Reset calendar to today"
msgstr "Pāriet uz šodienu"

#: declarativeimports/calendar/qml/MonthView.qml:282
#, kde-format
msgid "Next Month"
msgstr "Nākamais mēnesis"

#: declarativeimports/calendar/qml/MonthView.qml:284
#, kde-format
msgid "Next Year"
msgstr "Nākamais gads"

#: declarativeimports/calendar/qml/MonthView.qml:286
#, kde-format
msgid "Next Decade"
msgstr "Nākamā dekāde"

#: declarativeimports/calendar/qml/MonthView.qml:306
#, kde-format
msgid "Days"
msgstr ""

#: declarativeimports/calendar/qml/MonthView.qml:311
#, fuzzy, kde-format
#| msgid "Next Month"
msgid "Months"
msgstr "Nākamais mēnesis"

#: declarativeimports/calendar/qml/MonthView.qml:316
#, kde-format
msgid "Years"
msgstr ""

#: declarativeimports/plasmacomponents/qml/QueryDialog.qml:21
#, kde-format
msgid "OK"
msgstr "Labi"

#: declarativeimports/plasmacomponents/qml/QueryDialog.qml:22
#, kde-format
msgid "Cancel"
msgstr "Atcelt"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:80
#, kde-format
msgid "More actions"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:559
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:559
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:46
#, kde-format
msgid "Password"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:68
#, kde-format
msgid "Search…"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:70
#, kde-format
msgid "Search"
msgstr ""

#: plasma/applet.cpp:379
#, kde-format
msgid "Unknown"
msgstr "Nezināms"

#: plasma/applet.cpp:759
#, kde-format
msgid "Activate %1 Widget"
msgstr "Aktivēt %1 logdaļu"

#: plasma/containment.cpp:115 plasma/private/applet_p.cpp:106
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Noņemt %1"

#: plasma/containment.cpp:121 plasma/corona.cpp:406 plasma/corona.cpp:496
#, kde-format
msgid "Enter Edit Mode"
msgstr ""

#: plasma/containment.cpp:124 plasma/private/applet_p.cpp:111
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Konfigurēt %1..."

#: plasma/corona.cpp:355 plasma/corona.cpp:482
#, kde-format
msgid "Lock Widgets"
msgstr "Slēgt logdaļas"

#: plasma/corona.cpp:355
#, kde-format
msgid "Unlock Widgets"
msgstr "Atslēgt logdaļas"

#: plasma/corona.cpp:404
#, kde-format
msgid "Exit Edit Mode"
msgstr ""

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Vai izveidot tēmas kešatmiņu uz diska."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Maksimālais tēmas kešatmiņas izmērs uz diska kilobaitos. Ņemiet vērā, ka "
"parasti šīs datnes ir nelielas un tādēļ maksimālais izmērs netiks izmantots. "
"Tādēļ ir droši likt lielu izmēru."

#: plasma/packagestructure/packages.cpp:28
#: plasma/packagestructure/packages.cpp:47
#, kde-format
msgid "Main Script File"
msgstr "Galvenā skripta datne"

#: plasma/packagestructure/packages.cpp:29
#, kde-format
msgid "Tests"
msgstr "Tests"

#: plasma/packagestructure/packages.cpp:69
#, kde-format
msgid "Images"
msgstr "Attēli"

#: plasma/packagestructure/packages.cpp:70
#, kde-format
msgid "Themed Images"
msgstr "Attēli pa tēmu"

#: plasma/packagestructure/packages.cpp:76
#, kde-format
msgid "Configuration Definitions"
msgstr "Konfigurācijas definīcijas"

#: plasma/packagestructure/packages.cpp:81
#, kde-format
msgid "User Interface"
msgstr "Lietotāja saskarne"

#: plasma/packagestructure/packages.cpp:83
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:29
#, kde-format
msgid "Data Files"
msgstr "Datu datnes"

#: plasma/packagestructure/packages.cpp:85
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:31
#, kde-format
msgid "Executable Scripts"
msgstr "Izpildāmie skripti"

#: plasma/packagestructure/packages.cpp:89
#, kde-format
msgid "Screenshot"
msgstr "Ekrānuzņēmums"

#: plasma/packagestructure/packages.cpp:91
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:39
#, kde-format
msgid "Translations"
msgstr "Tulkojumi"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:30
#, kde-format
msgid "Configuration UI pages model"
msgstr "UI lapu konfigurācijas modelis"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:31
#, kde-format
msgid "Configuration XML file"
msgstr "Konfigurācijas XML datne"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:47
#, kde-format
msgid "Custom expander for compact applets"
msgstr "Pielāgots izpletējs kompaktām lietotnēm"

#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:36
#, kde-format
msgid "Service Descriptions"
msgstr "Servisu apraksti"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:30
#, kde-format
msgid "Images for dialogs"
msgstr "Attēli priekš dialogiem"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:31
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:32
#, kde-format
msgid "Generic dialog background"
msgstr "Vispārējs dialogu fons"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:33
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:34
#, kde-format
msgid "Theme for the logout dialog"
msgstr "Atteikšanās dialoga tēma"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:36
#, kde-format
msgid "Wallpaper packages"
msgstr "Tapešu pakotnes"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:38
#, kde-format
msgid "Images for widgets"
msgstr "Attēli priekš logdaļām"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:39
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:40
#, kde-format
msgid "Background image for widgets"
msgstr "Logdaļu fona attēls"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:41
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:42
#, kde-format
msgid "Analog clock face"
msgstr "Analogais pulkstenis"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:43
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:44
#, kde-format
msgid "Background image for panels"
msgstr "Paneļu fona attēls"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:45
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:46
#, kde-format
msgid "Background for graphing widgets"
msgstr "Zīmējošu logdaļu fons"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:47
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:48
#, kde-format
msgid "Background image for tooltips"
msgstr "Paskaidres fona attēls"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:50
#, kde-format
msgid "Opaque images for dialogs"
msgstr "Necaurspīdīgi attēli priekš dialogiem"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:51
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:52
#, kde-format
msgid "Opaque generic dialog background"
msgstr "Vispārējs necaurspīdīgs dialogu fons"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:55
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:58
#, kde-format
msgid "Opaque theme for the logout dialog"
msgstr "Necaurspīdīga tēma atteikšanās dialogam"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:60
#, kde-format
msgid "Opaque images for widgets"
msgstr "Necaurspīdīgi attēli logdaļām"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:63
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:66
#, kde-format
msgid "Opaque background image for panels"
msgstr "Necaurspīdīgi attēli paneļiem"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:67
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:68
#, kde-format
msgid "Opaque background image for tooltips"
msgstr "Necaurspīdīgi attēli paskaidrēm"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:70
#, kde-format
msgid "KColorScheme configuration file"
msgstr "KColorScheme konfigurācijas datne"

#: plasma/pluginloader.cpp:592 plasma/pluginloader.cpp:593
#, kde-format
msgctxt "misc category"
msgid "Miscellaneous"
msgstr "Dažādi"

#: plasma/private/applet_p.cpp:126
#, kde-format
msgid "The %1 widget did not define which ScriptEngine to use."
msgstr "Logdaļa %1 nav definējusi izmantojamo ScriptEngine."

#: plasma/private/applet_p.cpp:145
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr ""
"Nebija iespējams atvērt %1 pakotni, kas ir nepieciešama priekš logdaļas %2."

#: plasma/private/applet_p.cpp:166
#, kde-format
msgctxt ""
"API or programming language the widget was written in, name of the widget"
msgid "Could not create a %1 ScriptEngine for the %2 widget."
msgstr "Nebija iespējams izveidot %1 ScriptEngine priekš logdaļas %2."

#: plasma/private/applet_p.cpp:172
#, kde-format
msgid "Show Alternatives..."
msgstr "Parādīt alternatīvas..."

#: plasma/private/applet_p.cpp:274
#, kde-format
msgid "Widget Removed"
msgstr "Logdaļa ir noņemta"

#: plasma/private/applet_p.cpp:275
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Logdaļa „%1“ ir noņemta."

#: plasma/private/applet_p.cpp:279
#, kde-format
msgid "Panel Removed"
msgstr "Panelis ir noņemts"

#: plasma/private/applet_p.cpp:280
#, kde-format
msgid "A panel has been removed."
msgstr "Panelis ir noņemts."

#: plasma/private/applet_p.cpp:283
#, kde-format
msgid "Desktop Removed"
msgstr "Darbvirsma ir noņemta"

#: plasma/private/applet_p.cpp:284
#, kde-format
msgid "A desktop has been removed."
msgstr "Darbvirsma ir noņemta"

#: plasma/private/applet_p.cpp:287
#, kde-format
msgid "Undo"
msgstr "Atdarīt"

#: plasma/private/applet_p.cpp:375
#, kde-format
msgid "Widget Settings"
msgstr "Logdaļas iestatījumi"

#: plasma/private/applet_p.cpp:382
#, kde-format
msgid "Remove this Widget"
msgstr "Noņemt šo logdaļu"

#: plasma/private/applet_p.cpp:389
#: plasma/private/associatedapplicationmanager.cpp:65
#: plasma/private/associatedapplicationmanager.cpp:124
#: plasma/private/associatedapplicationmanager.cpp:166
#, kde-format
msgid "Run the Associated Application"
msgstr "Darbināt piesaistīto aplikāciju"

#: plasma/private/applet_p.cpp:480
#, kde-format
msgid "Script initialization failed"
msgstr "Nesekmīga skripta inicializācija"

#: plasma/private/associatedapplicationmanager.cpp:62
#: plasma/private/associatedapplicationmanager.cpp:163
#, kde-format
msgid "Open with %1"
msgstr "Atvērt ar %1"

#: plasma/private/containment_p.cpp:57
#, kde-format
msgid "Remove this Panel"
msgstr "Noņemt šo paneli"

#: plasma/private/containment_p.cpp:59
#, kde-format
msgid "Remove this Activity"
msgstr "Noņemt šo aktivitāti"

#: plasma/private/containment_p.cpp:65
#, kde-format
msgid "Activity Settings"
msgstr "Aktivitātes iestatījumi"

#: plasma/private/containment_p.cpp:71
#, kde-format
msgid "Add Widgets..."
msgstr "Pievienot logdaļas..."

#: plasma/private/containment_p.cpp:187
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Nebija iespējams atrast pieprasīto komponenti: %1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"Logdaļas kopīgošana tīklā ļauj citiem datoriem piekļūt šai logdaļai un "
"tādējādi kalpot kā tālvadības pultīm."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Koplietot šo logdaļu tīklā"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr "Atļaut jebkuram brīvi piekļūt šai logdaļai."

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Nederīgs (null) serviss. Nav iespējams veikt darbības."

#: plasmaquick/appletquickitem.cpp:609
#, fuzzy, kde-format
#| msgid "Unknown"
msgid "Unknown Applet"
msgstr "Nezināms"

#: plasmaquick/appletquickitem.cpp:624
#, fuzzy, kde-format
#| msgid "Error loading QML file: %1"
msgid "Error loading QML file: %1 %2"
msgstr "Kļūda ielādējot QML datni: %1"

#: plasmaquick/appletquickitem.cpp:626
#, kde-format
msgid "Error loading Applet: package inexistent. %1"
msgstr "Kļūda ielādējot lietotni: pakotne neeksistē. %1"

#: plasmaquick/configview.cpp:92
#, kde-format
msgid "%1 Settings"
msgstr "%1 iestatījumi"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:603
#, kde-format
msgid "Plasma Package"
msgstr "Plasma pakotne"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:607
#, kde-format
msgid "Install"
msgstr "Instalēt"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:620
#, kde-format
msgid "Package Installation Failed"
msgstr "Neizdevās instalēt pakotni"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:643
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "Nomestā pakotne nav derīga."

#: scriptengines/qml/plasmoid/containmentinterface.cpp:652
#: scriptengines/qml/plasmoid/containmentinterface.cpp:721
#, kde-format
msgid "Widgets"
msgstr "Logdaļas"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:657
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Pievienot %1"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:671
#: scriptengines/qml/plasmoid/containmentinterface.cpp:725
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Pievienot ikonu"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:683
#, kde-format
msgid "Wallpaper"
msgstr "Tapete"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:693
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Uzlikt %1"

#: scriptengines/qml/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Saturs ir nomests"

#~ msgid "Accessibility"
#~ msgstr "Pieejamība"

#~ msgid "Application Launchers"
#~ msgstr "Programmu palaidēji"

#~ msgid "Astronomy"
#~ msgstr "Astronomija"

#~ msgid "Date and Time"
#~ msgstr "Datums un laiks"

#~ msgid "Development Tools"
#~ msgstr "Izstrādes rīki"

#~ msgid "Education"
#~ msgstr "Izglītība"

#~ msgid "Environment and Weather"
#~ msgstr "Vide un laikapstākļi"

#~ msgid "Examples"
#~ msgstr "Piemēri"

#~ msgid "File System"
#~ msgstr "Datņu sistēma"

#~ msgid "Fun and Games"
#~ msgstr "Spēles un jautrība"

#~ msgid "Graphics"
#~ msgstr "Grafika"

#~ msgid "Language"
#~ msgstr "Valoda"

#~ msgid "Mapping"
#~ msgstr "Kartēšana"

#~ msgid "Miscellaneous"
#~ msgstr "Dažādi"

#~ msgid "Multimedia"
#~ msgstr "Multivide"

#~ msgid "Online Services"
#~ msgstr "Tiešsaistes servisi"

#~ msgid "Productivity"
#~ msgstr "Produktivitāte"

#~ msgid "System Information"
#~ msgstr "Sistēmas informācija"

#~ msgid "Utilities"
#~ msgstr "Utilītas"

#~ msgid "Windows and Tasks"
#~ msgstr "Logi un uzdevumi"

#~ msgid "Clipboard"
#~ msgstr "Starpliktuve"

#~ msgid "Tasks"
#~ msgstr "Uzdevumi"

#~ msgctxt "%1 is the name of the containment"
#~ msgid "Edit %1..."
#~ msgstr "Rediģēt %1..."

#~ msgid "Default settings for theme, etc."
#~ msgstr "Tēmu u.c. noklusējuma iestatījumi."

#~ msgid "Color scheme to use for applications."
#~ msgstr "Lietotņu krāsu shēma."

#~ msgid "Preview Images"
#~ msgstr "Priekšskatīt attēlus"

#~ msgid "Preview for the Login Manager"
#~ msgstr "Pieteikšanās pārvaldnieka priekšskatījums"

#~ msgid "Preview for the Lock Screen"
#~ msgstr "Ekrānslēdzēja priekšskatījums"

#~ msgid "Preview for the Userswitcher"
#~ msgstr "Lietotāju pārslēdzēja priekšskatījums"

#~ msgid "Preview for the Virtual Desktop Switcher"
#~ msgstr "Virtuālo darbvirsmu pārslēdzēja priekšskatījums"

#~ msgid "Preview for Splash Screen"
#~ msgstr "Uzplaiksnījumekrāna priekšskatījums"

#~ msgid "Preview for KRunner"
#~ msgstr "KRunner priekšskatījums"

#~ msgid "Preview for the Window Decorations"
#~ msgstr "Logu dekorāciju priekšskatījums"

#~ msgid "Preview for Window Switcher"
#~ msgstr "Logu pārslēdzēja priekšskatījums"

#~ msgid "Login Manager"
#~ msgstr "Pieteikšanās pārvaldnieks"

#~ msgid "Main Script for Login Manager"
#~ msgstr "Galvenais pieteikšanās pārvaldnieka skripts"

#~ msgid "Logout Dialog"
#~ msgstr "Atteikšanās dialogs"

#~ msgid "Main Script for Logout Dialog"
#~ msgstr "Galvenais atteikšanās dialoga skripts"

#~ msgid "Screenlocker"
#~ msgstr "Ekrānslēdzējs"

#~ msgid "Main Script for Lock Screen"
#~ msgstr "Galvenais ekrānslēdzēja skripts"

#~ msgid "UI for fast user switching"
#~ msgstr "Ātrs lietotāju pārslēdzējs"

#~ msgid "Main Script for User Switcher"
#~ msgstr "Galvenais lietotāju pārslēdzēja skripts"

#~ msgid "Virtual Desktop Switcher"
#~ msgstr "Virtuālo darbvirsmu pārslēdzējs"

#~ msgid "Main Script for Virtual Desktop Switcher"
#~ msgstr "Galvenais virtuālo darbvirsmu pārslēdzēja skripts"

#~ msgid "On-Screen Display Notifications"
#~ msgstr "Uz ekrāna rādāmie paziņojumi"

#~ msgid "Main Script for On-Screen Display Notifications"
#~ msgstr "Galvenais uz ekrāna rādāmo paziņojumu skripts"

#~ msgid "Splash Screen"
#~ msgstr "Uzplaiksnījuma ekrāns"

#~ msgid "Main Script for Splash Screen"
#~ msgstr "Galvenais uzplaiksnījuma ekrāna skripts"

#~ msgid "KRunner UI"
#~ msgstr "KRunner UI"

#~ msgid "Main Script KRunner"
#~ msgstr "Galvenais KRunner skripts"

#~ msgid "Window Decoration"
#~ msgstr "Loga dekorācija"

#~ msgid "Main Script for Window Decoration"
#~ msgstr "Galvenais logu dekorāciju skripts"

#~ msgid "Window Switcher"
#~ msgstr "Logu pārslēdzējs"

#~ msgid "Main Script for Window Switcher"
#~ msgstr "Galvenais logu pārslēdzēja skripts"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Pabeigt pielāgot izkārtojumu"

#~ msgid "Customize Layout..."
#~ msgstr "Pielāgot izkārtojumu..."

#~ msgid "Fetching file type..."
#~ msgstr "Iegūst datnes tipu..."
